<?php

namespace App;

class Deck
{
    /**
     * @var array
     */
    protected $cards;
    /**
     * @var int
     */
    protected $currentCardIndex;

    /**
     * Deck constructor.
     *
     * @param array $cards
     * @param int   $currentCardIndex
     */
    public function __construct(array $cards, int $currentCardIndex = 0)
    {
        $this->cards = $cards;

        $this->currentCardIndex = $currentCardIndex;
    }

    /**
     * Draws cards Cyclically
     *
     * @return string
     */
    public function draw()
    {
        $deckCount = $this->deckCount();
        $currentCard = $this->cards[$this->currentCardIndex % $deckCount];

        //increment the card index
        $this->currentCardIndex = ($this->currentCardIndex + 1) % $deckCount;

        return $currentCard;
    }

    /**
     * Calculate chance in percent of drafting a card amongst deck left
     *
     * @return
     */
    public function getChance()
    {
        $cardsLeft = $this->deckCount() - $this->currentCardIndex;

        return number_format( 1 / $cardsLeft * 100, 2 );
    }

    /**
     * @return int
     */
    private function deckCount()
    {
        return count($this->cards);
    }
}
