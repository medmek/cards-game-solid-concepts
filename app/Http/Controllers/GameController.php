<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\CardForm;
use App\Services\CardSetProviderInterface;
use App\Services\DeckInterface;
use App\Deck;

class GameController extends BaseController
{
    /**
     * @var CardSetProviderInterface
     */
    protected $cardSetProvider;

    /**
     * @var DeckInterface
     */
    protected $deckService;

    /**
     * GameController constructor.
     *
     * @param CardSetProviderInterface $cardSetProvider
     * @param DeckInterface            $deckService
     */
    public function __construct(CardSetProviderInterface $cardSetProvider, DeckInterface $deckService)
    {
        /** @var \App\Services\BJ52DeckProvider $cardSetProvider */
        $this->cardSetProvider = $cardSetProvider;

        /** @var \App\Services\DeckService $deckService */
        $this->deckService = $deckService;
    }

    /**
     * Show one of the games steps based on session.
     *
     * @param FormBuilder $formBuilder
     *
     * @return View
     */
    public function chooseCard(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(CardForm::class, [
            'method'    => 'POST',
            'url'       => action('GameController@drawCard'),
            'model'     => [
                'cardSet' => [
                    'ranks' => $this->cardSetProvider->getRanks(),
                    'suits' => $this->cardSetProvider->getSuits(),
                    ]
            ]
        ]);

        return view('card.select', compact('form'));
    }

    /**
     * @param Request     $request
     * @param FormBuilder $formBuilder
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function drawCard(Request $request, FormBuilder $formBuilder)
    {
        $selectedSuit   = $request->get('suit');
        $selectedRank   = $request->get('rank');

        /** @var Session $session */
        $session        = $request->session();

        //generate new deck for the new card selection
        if($selectedSuit && $selectedRank){
            $deck = new Deck($this->deckService->generateRandomDeck());

            $selectedCard = $selectedSuit . $selectedRank;
            $session->put('selectedCard', $selectedCard);
        } else {
            $deck = $session->get('deck');
        }

        $selectedCard = $session->get('selectedCard');

        //calculates chance of getting a card from the current deck
        $chance = $deck->getChance();

        //draw and check if match selected
        $session->put('cardFound', $deck->draw() === $selectedCard);
        $session->put('deck', $deck);

        //generate simple form
        $form = $formBuilder->plain([
            'method'    => 'POST',
            'url'       => action('GameController@drawCard')
        ])
            ->add('submit', 'submit', ['label' => 'Draw']);

        return view('card.draw', compact(['form', 'chance','selectedCard']));
    }
}