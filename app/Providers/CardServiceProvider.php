<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CardServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Services\CardSetProviderInterface::class, function ($app) {
            return new \App\Services\BJ52DeckProvider();
        });

        $this->app->bind(\App\Services\DeckInterface::class, function ($app) {
            return new \App\Services\DeckService($app->make('\App\Services\BJ52DeckProvider'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
