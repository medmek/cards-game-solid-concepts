<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CardForm extends Form
{
    public function getSuits(){
        $suits = $this->model['cardSet']['suits'];
        return $suits;
    }

    public function getRanks(){

        $ranks = $this->model['cardSet']['ranks'];
        return $ranks;
    }

    public function buildForm()
    {
        $this
            ->add('suit', 'select',
                [
                    'choices' => $this->getSuits(),
                    'empty_value' => '=== Select a suit ===',
                    'rules' => 'required'
                ])
            ->add('rank', 'select',
                [
                    'choices' => $this->getRanks(),
                    'empty_value' => '=== Select a rank ===',
                    'rules' => 'required'
                ])
            ->add('submit', 'submit', ['label' => 'Calculate %'])
            ->add('clear', 'reset', ['label' => 'Clear form']);
    }
}
