<?php

namespace App\Services;

class BJ52DeckProvider implements CardSetProviderInterface
{
    protected $cardSet;

    const SUITS =
        [
            'H' => 'Hearts ♥',
            'S' => 'Spades ♠',
            'D' => 'Diamonds ♦',
            'C' => 'Clubs ♣'
        ];

    const RANKS =
        [
            'A'     => 'A',
            '2'     => '2',
            '3'     => '3',
            '4'     => '4',
            '5'     => '5',
            '6'     => '6',
            '7'     => '7',
            '8'     => '8',
            '9'     => '9',
            '10'    => '10',
            'J'     => 'J',
            'Q'     => 'Q',
            'K'     => 'K'
        ];

    /**
     * Get a card set from combining suits and ranks
     *
     * @return array
     */
    public function getCardSet()
    {
        if(!isset($this->cardSet)){
            $this->cardSet = $this->array_combine(self::SUITS, self::RANKS);
        }

        return $this->cardSet;
    }

    /**
     * Naive implementation of cartesian product
     *
     * @param array $a
     * @param array $b
     *
     * @return array
     */
    private function array_combine(array $a, array $b)
    {
        $result = [];

        foreach ($a as $aKey => $aValue) {
            foreach ($b as $bKey => $bValue) {
                $result[] = $aKey . $bKey;
            }
        }

        return $result;
    }

    /**
     * Returns array of available wildcards in the card set
     * to demonstrate extensibility
     *
     * @return array
     */
    public function getWildCards()
    {
        return [];
    }

    /**
     * Return suits that constitutes the card set
     *
     * @return array
     */
    public function getSuits()
    {
        return self::SUITS;
    }

    /**
     * return ranks that constitutes the card set
     *
     * @return array
     */
    public function getRanks()
    {
        return self::RANKS;
    }
}