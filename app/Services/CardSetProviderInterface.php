<?php


namespace App\Services;


interface CardSetProviderInterface
{
    /**
     * Get a card set from combining suits and ranks
     *
     * @return array
     */
    public function getCardSet();

    /**
     * Return suits that constitutes the card set
     *
     * @return array
     */
    public function getSuits();

    /**
     * Return ranks that constitutes the card set
     *
     * @return array
     */
    public function getRanks();

    /**
     * Return array of available wildcards in the card set
     * to demonstrate extensibility
     *
     * @return array
     */
    public function getWildCards();
}