<?php


namespace App\Services;


interface DeckInterface
{
    /**
     * get random deck based on a card set
     * @return mixed
     */
    public function generateRandomDeck();
}