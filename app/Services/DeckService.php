<?php

namespace App\Services;

class DeckService implements DeckInterface
{
    protected $cardSet;

    public function __construct(CardSetProviderInterface $cardSetProvider)
    {
        /** @var BJ52DeckProvider $cardSetProvider */
        $this->cardSet = $cardSetProvider->getCardSet();
    }

    /**
     * Combines cards randomly to create deck
     *
     * @return array
     */
    public function generateRandomDeck()
    {
        shuffle($this->cardSet);

        return $this->cardSet;
    }
}