## About Card Drafter

A web application built using Laravel PHP framework, to showcase SOLID principles, in a simple card game logic :

- User chooses a card.
- App generates random deck.
- User starts drafting cards, one by one.
- Website should display a chance of getting customer selected card on the next Draft.

If customer selected card is drafted app should display popup with a message "Got it, the chance was
  (current chance of getting the card)%"

## Installation

### Prerequisites
To be run ideally in `Linux`, `MacOS` or `Vagrant` for windows users

#### Windows users (non-Vagrant):
- Download wamp: http://www.wampserver.com/en/
- Download and extract cmder mini: https://github.com/cmderdev/cmder/releases/download/v1.1.4.1/cmder_mini.zip
- Update windows environment variable path to point to your php install folder (inside wamp installation dir) (here is how you can do this http://stackoverflow.com/questions/17727436/how-to-properly-set-php-environment-variable-to-run-commands-in-git-bash)

#### Mac Os, Ubuntu and windows users continue here: 
- Download composer https://getcomposer.org/download/
- Clone the project somewhere in your environment
    ```
    git clone https://gitlab.com/medmek/cards-game-solid-concepts.git [project-root-path-here]
    ```
- Open the console and cd to your project root directory
- Run `composer install` or ```php composer.phar install```
- Run `php artisan serve`

## Tests
Using phpUnit provided in vendor, run `Unit` test suite
```
./vendor/bin/phpunit --testsuite Unit
```

## Contributing

Thank you for considering contributing to this project! Please open issue which describes the issue/improvement in mind

## License

This open-source software is licensed under the [MIT license](https://opensource.org/licenses/MIT).
