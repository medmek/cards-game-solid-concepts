<?php

namespace App;

use Tests\TestCase;

class DeckTest extends TestCase
{

    public function testGetChanceOnSimpleDeck()
    {
        $simpleCardSet = ['1', '2'];
        $simpleDeck = new Deck($simpleCardSet);

        $this->assertEquals('50.00', $simpleDeck->getChance());
    }

    public function testGetChanceAfterDrafting()
    {
        $anotherCardSet = ['1', '2', '3', '4', '5'];

        $anotherDeck = new Deck($anotherCardSet);

        //this draw leaves the deck with only 4 cards
        $anotherDeck->draw();

        $this->assertEquals('25.00', $anotherDeck->getChance());
    }
}
