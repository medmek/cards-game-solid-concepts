<?php

namespace App;

use Tests\TestCase;
use App\Services\BJ52DeckProvider;

class BJ52DeckProviderTest extends TestCase
{

    /**
     * @var BJ52DeckProvider
     */
    private $bjDeckProvider;

    public function setUp(): void
    {
        $this->bjDeckProvider = new BJ52DeckProvider();
    }

    public function testGetCardSetCount()
    {
        $this->assertCount(52, $this->bjDeckProvider->getCardSet());
    }
}
