@extends('card.base')

@section('content')
    <div class="title m-b-md">
        Card Probability Calculator
    </div>

    {!! form($form) !!}
@endsection