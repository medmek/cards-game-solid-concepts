@extends('card.base')

@section('content')
    <div class="title m-b-md">
        Card Probability Calculator
    </div>

    @if (Session::get('cardFound'))
        <script type="text/javascript">
            alert('Got it, the chance was {{ $chance }}%');
            location='{{ action('GameController@drawCard') }}';
        </script>
    @else
        <div class="chance">Chance of getting "{{ $selectedCard }}" is {{ $chance }}%</div>

        {!! form($form) !!}
    @endif
@endsection